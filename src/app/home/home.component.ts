import {
  Component,
  OnInit
} from '@angular/core';

import { AppState } from '../app.service';
import { Title } from './title';
import { XLargeDirective } from './x-large';

@Component({
  /**
   * The selector is what angular internally uses
   * for `document.querySelectorAll(selector)` in our index.html
   * where, in this case, selector is the string 'home'.
   */
  selector: 'home',  // <home></home>
  /**
   * We need to tell Angular's Dependency Injection which providers are in our app.
   */
  providers: [
    Title
  ],
  /**
   * Our list of styles in our component. We may add more to compose many styles together.
   */
  styleUrls: [ './home.component.css' ],
  /**
   * Every Angular template is first compiled by the browser before Angular runs it's compiler.
   */
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  /**
   * Set our default values
   */
  public localState = { value: '' };
  boardData: Object;
  public createTaskBoard:boolean=false;
  boardDetailsData: Object;
  public displayType:number=1;
  viewClass: number =1;
  editList:number=0;
  boardListData: any;
  boardTaskData=[];
  dashboardName:any;
  listName:any;
  canEdit: number=0;
  showId: any;
  taskName:any;
  createTask:number=0;
  createTaskView: any;
  createTaskName:any;
  viewDeleteButton:number=0;
  deleteListId: any;
  /**
   * TypeScript public modifiers
   */
  constructor(
    public appState: AppState,
    public title: Title
  ) {}

  public ngOnInit() {
    console.log('hello `Home` component');
   this.getAllBoards();
    /**
     * this.title.getData().subscribe(data => this.data = data);
     */
  }

  public submitState(value: string) {
    console.log('submitState', value);
    this.appState.set('value', value);
    this.localState.value = '';
  }
  showTask(){
    this.createTaskBoard=!this.createTaskBoard;
  }
  getAllBoards(){
    this.title.getDashboardData().subscribe(res=>{
      console.log(res);
      if(res){
        this.boardData=res;
      }
    })

  }
  getBoardDetails(id){
    this.title.getDataBoardDetails(id).subscribe(res=>{
      console.log(res);
      if(res){
        this.displayType=2;
        this.boardDetailsData=res;
        this.getListData(id);
      }
    })
  }
  getListData(id){
    this.title.getListData(id).subscribe(res=>{
      console.log(res);
      if(res){
        this.boardListData=res;
        for(let i=0;i<this.boardListData.lists.length;i++){
          this.getTaskData(this.boardListData.lists[i]._id,i);
        }
        
      }
    })

  }
  changeClass(){
    this.canEdit=1;
  }
  changeView(item){
    console.log(item);
    this.deleteListId=item._id;
    this.viewDeleteButton=1;
    console.log(this.viewDeleteButton);
  }
  deleteListDetails(id,index){
    this.title.deleteTaskDetails(id,this.boardDetailsData).subscribe(res=>{
      console.log(res);
      if(res){
        this.boardListData.lists.splice(index,1);
        // this.getAllBoards();

      }
    })
  }
  getTaskData(id,index){
    this.title.getTasketails(id).subscribe(res=>{
      console.log(res);
      if(res){
        this.boardListData.lists[index].taskData=res;
        
        console.log(this.boardListData);

      }
    })

  }
  createDashboard(){
    console.log(this.dashboardName);
    this.title.createDashboard(this.dashboardName).subscribe(res=>{
      console.log(res);
      if(res){
        this.getAllBoards();
        this.showTask();
        // this.boardTaskData=res;
      }
    })
  }
  deleteBoard(id){
    this.title.deleteBoard(id).subscribe(res=>{
      console.log(res);
      if(res){
        this.getAllBoards();

      }
    })

  }
  saveList(id){
    this.title.createList(this.listName,id).subscribe(res=>{
      console.log(res);
      if(res){
        this.getListData(id);
        this.viewClass=1;
        this.listName='';
        // this.boardTaskData=res;
      }
    })

  }
  changeViewClass(){
    this.viewClass=2;
  }
  editListShow(item){
    console.log(item);
    this.showId=item._id;
    this.editList=1;

  }
  saveTask(item){
    this.title.updateTask(item).subscribe(res=>{
      console.log(res);
      if(res){
this.editList=0;
      }
    })

  }
  deleteTask(item,data,index){
console.log(item);
console.log(data);
this.title.deleteTask(data,item._id).subscribe(res=>{
  console.log(res);
  if(res){
    this.getTaskData(item._id,index);
this.hideMask();
    // this.getAllBoards();

  }
})

  }
  createTaskData(item){
    console.log(item);
    this.createTaskView=item._id;
    this.createTask=1;
  }
  hideMask(){
    this.editList=0;
    this.createTask=0;
  }
  createNewTask(item,index){
    console.log(item);
    console.log(this.createTaskName);
    this.title.createTaskLists(this.createTaskName,item._id).subscribe(res=>{
      console.log(res);
      if(res){
        this.createTask=0;
        this.createTaskName='';
        this.getTaskData(item._id,index);

      }
    })

  }
}
