import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';
@Injectable()
export class Title {

  public value = 'Angular 2';
  public baseUrl='https://mean-trello-clone.herokuapp.com/'
  constructor(
    public http: HttpClient,
  ) { }

  public getData() {
    console.log('Title#getData(): Get Data');
    return this.http.get('/assets/data.json');
  }
public getDashboardData(){
  return this.http.get(this.baseUrl + 'boards/getBoards')
  .map(res=>res);
}
public getDataBoardDetails(id){
  return this.http.get(this.baseUrl + 'boards/getBoard/'+id)
  .map(res=>res);

}
public getListData(id){
  return this.http.get(this.baseUrl + 'lists/getList/'+id)
  .map(res=>res);

}
public getTasketails(id){
  return this.http.get(this.baseUrl + 'tasks/getTasks/'+id)
  .map(res=>res);
}

public createDashboard(name){
  let body : any = {};
    body.board_name = name;
  return this.http.post(this.baseUrl + 'boards/createBoard',body)
  .map(res=>res);
}

public createList(name,id){
  let body : any = {};
    body.list_name = name;
  return this.http.put(this.baseUrl + 'lists/create/'+id,body)
  .map(res=>res);
}
public createTaskLists(name,id){
  let body : any = {};
  body.task_name = name;
return this.http.post(this.baseUrl + 'tasks/create/'+id,body)
.map(res=>res);
}

public deleteBoard(id){
  return this.http.delete(this.baseUrl + 'boards/deleteBoard/'+id)
  .map(res=>res);
}
public deleteTaskDetails(id1,id2){
  return this.http.put(this.baseUrl + 'lists/delete/'+id2._id+'/'+id1,'')
  .map(res=>res);
}
public deleteTask(id1,id2){
  return this.http.put(this.baseUrl + 'tasks/delete/'+id1+'/'+id2,'')
  .map(res=>res);
}

public updateTask(item){
  let body : any = {};
    body.task_name = item.task_name;
  return this.http.put(this.baseUrl + 'tasks/update/'+item._id,body)
  .map(res=>res);
}
}
